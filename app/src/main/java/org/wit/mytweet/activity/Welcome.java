package org.wit.mytweet.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


import org.wit.mytweet.R;

public class Welcome extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void loginPressed (View view)  {startActivity (new Intent(this, Login.class));
    }

    public void signupPressed (View view) {startActivity (new Intent(this, Signup.class));
    }
}