package org.wit.mytweet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.wit.mytweet.Application.MyTweetApp;
import org.wit.mytweet.R;
import org.wit.mytweet.models.Users;

//import static org.wit.mytweet.Intent.IntentHelper.navigateUp;


public class Signup extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;

    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = MyTweetApp.getApp();
        setContentView(R.layout.activity_signup);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }


    public void signupPressed (View view)
    {

         firstName = (EditText) findViewById(R.id.signupFirstname);
         lastName = (EditText) findViewById(R.id.signupLastName);
         email = (EditText) findViewById(R.id.signupEmail);
         password = (EditText) findViewById(R.id.signupPassword);

        Users user = new Users(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

        app = MyTweetApp.getApp();
        app.newUser(user);

        Toast toast = Toast.makeText(this, "You have been Registered!", Toast.LENGTH_SHORT);
        toast.show();

        startActivity (new Intent(this, Login.class));
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
