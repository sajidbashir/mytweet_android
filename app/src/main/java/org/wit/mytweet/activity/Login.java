package org.wit.mytweet.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;

import android.widget.Toast;

import org.wit.mytweet.Application.MyTweetApp;
import org.wit.mytweet.R;

public class Login extends AppCompatActivity {
    private MyTweetApp app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = MyTweetApp.getApp();
        setContentView(R.layout.activity_login);
        //app = MyTweetApp.getApp();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void loginPressed(View view) {
        EditText email = (EditText) findViewById(R.id.loginEmail);
        EditText password = (EditText) findViewById(R.id.loginPassword);
        MyTweetApp app = MyTweetApp.getApp();
        if (app.isUserValid(email.getText().toString(), password.getText().toString())) {
            Toast toast = Toast.makeText(this, "Login Pressed!", Toast.LENGTH_SHORT);
            toast.show();
            startActivity(new Intent(this, MyTweet.class));
        } else {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            toast.show();
            startActivity(new Intent(this, Signup.class));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}