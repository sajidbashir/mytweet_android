package org.wit.mytweet.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.wit.mytweet.Application.MyTweetApp;
import org.wit.mytweet.R;
//import org.wit.mytweet.models.TweetMsg;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

//import static org.wit.mytweet.Intent.IntentHelper.navigateUp;


public class MyTweet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytweet);

        TextView tv = (TextView) findViewById(R.id.textview_date);

        String dt;
        Date cal = (Date) Calendar.getInstance().getTime();
        dt = cal.toLocaleString();
        assert tv != null;
        tv.setText(dt.toString());

        final EditText et = (EditText)findViewById(R.id.tweetMsg);
        assert et != null;
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                TextView tv = (TextView)findViewById(R.id.counterMsg);
                assert tv != null;
                tv.setText(String.valueOf(140 - et.length()));
            }

            @Override
            public void onTextChanged(CharSequence s, int st, int b, int c)
            { }
            @Override
            public void beforeTextChanged(CharSequence s, int st, int c, int a)
            { }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {

            case android.R.id.home:  navigateUp(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}