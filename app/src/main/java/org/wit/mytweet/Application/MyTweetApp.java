package org.wit.mytweet.Application;

import android.app.Application;
import android.util.Log;

//import org.wit.mytweet.Intent.DbHelper;
//import org.wit.mytweet.models.TweetMsg;
import org.wit.mytweet.models.Users;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Admin on 17/10/2016.
 */

public class MyTweetApp extends Application {
    protected static MyTweetApp app;
    public List<Users> users = new ArrayList<Users>();

    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public void newUser(Users user) {

        users.add(user);
    }

    public static MyTweetApp getApp() {
        return app;
    }

    public boolean isUserValid(String email, String password) {
        for (Users user : users) {
            if (user.email.equals(email) && user.password.equals(password)) {
                return true;
            }
        }
        return false;
    }

}